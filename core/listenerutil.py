import os
import sys
import multiprocessing

from core.httplistener import *
from core.util import *

def startHttpListener(*args):

    lName = args[0]
    lPort = args[1]
    lIp   = args[2]
    lDm   = args[3]
    
    l = httpListener(lName, lPort, lIp, lDm)
    l.setup()
    l.start()
    s("http listener {} started".format(lName))