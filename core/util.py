import platform
import sys

def s(msg):
    print(f"Sucess: {msg}")

def e(msg):
    print(f"Error: {msg}")

def sysCheck():
    sys = []
    sys.append(checkOS())
    sys.append(isx64())
    return sys

def checkOS():
    os = platform.platform()
    return os

def isx64():
    if sys.maxsize > 2 ** 32:
        arch = "x86_64"
    else:
        arch = "x86"
    return arch


