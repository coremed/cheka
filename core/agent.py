import os
import sys

class agent:
	
	"""agent 
	
	This class allows operators to task agents.
	Agents can be tasked with sleep time, shell,
	and powershell.
	"""
	
	def __init__(self, name, listener, ipaddr, hostname, rType, key):
		
		self.name     = name
		self.listener = listener
		self.ipaddr   = ipaddr
		self.hostname = hostname
		self.rType    = rType
		self.key      = key
		self.sleep    = sleep
		self.path = "data/listeners/{}/agents/{}/".format(self.listener, self.name)
		self.tasksPath = "{}tasks".format(self.path, self.name)
		
		
		# this is far from ready but the idea is here
		# TODO: actually add the functionality
