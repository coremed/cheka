# cheka C2

cheka C2 is a basic command and control framework written in python3 

## Authors

Ben Dattilio [@coremed](https://gitlab.com/coremed)

Will Im [@willjcim](https://gitlab.com/willjcim)

## Acknowledgments

[0xRick](https://github.com/0xRick)

# NO LONGER ACTIVE

cheka c2 has been merged into Clyde C2
